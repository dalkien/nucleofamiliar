package org.nexos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonUpdate {

    private String firstName;
    private String lastName;
    private String age;
    private String address;
    private String telephone;
    private String status;
    private String email;
    private List<Familiar> familiars;
}
