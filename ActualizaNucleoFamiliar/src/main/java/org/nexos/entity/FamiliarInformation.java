package org.nexos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "familiar_information")
public class FamiliarInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_familiar")
    private int id_familiar;

    @Column(name = "type_id",  unique = true)
    private String typeId;

    @Column(name = "number_id", unique = true)
    private String numberId;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "age")
    private int age;

    @Column(name = "code_relationship", nullable = false)
    private String codeRelationship;

}
