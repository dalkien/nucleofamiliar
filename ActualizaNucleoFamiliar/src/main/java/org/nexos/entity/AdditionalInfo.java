package org.nexos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "additional_info")
public class AdditionalInfo {

    @Id
    //@SequenceGenerator(name = "additional_info_id_generator", sequenceName = "additional_info_id_seq")
    //@GeneratedValue(generator = "additional_info_id_generator")
    @Column(name = "id_info")
    private int idInfo;

    @Column(nullable = false, name = "id_person")
    private int idPerson;

    @Column(name = "address")
    private String address;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "email")
    private String email;

}
