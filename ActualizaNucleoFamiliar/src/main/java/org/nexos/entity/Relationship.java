package org.nexos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "relationship")
public class Relationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_relation")
    private int idRelation;

    @Column(name = "type_id_principal")
    private String typeIdPrincipal;

    @Column(name = "number_id_principal")
    private String numberIdPrincipal;

    @Column(name = "type_id_family")
    private String typeIdFamily;

    @Column(name = "number_id_family")
    private String numberIdFamily;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "type_id_family", referencedColumnName = "type_id",
                    insertable = false, updatable = false),
            @JoinColumn(name = "number_id_family", referencedColumnName = "number_id",
                    insertable = false, updatable = false)
    })
    private FamiliarInformation familiarInformation;
}
