package org.nexos.repo;

import org.nexos.entity.Relationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RelationshipRepository extends JpaRepository<Relationship,Long> {

    List<Relationship> findByTypeIdPrincipalAndNumberIdPrincipal(String type, String number);

}
