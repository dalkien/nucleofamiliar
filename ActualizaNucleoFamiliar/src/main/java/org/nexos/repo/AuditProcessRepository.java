package org.nexos.repo;

import org.nexos.entity.AuditProcess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditProcessRepository extends JpaRepository<AuditProcess,Long> {
}
