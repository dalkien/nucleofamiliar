package org.nexos.repo;

import org.nexos.entity.FamiliarInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FamiliarInformationRepository extends JpaRepository<FamiliarInformation, Long> {
    Optional<FamiliarInformation> findByTypeIdAndNumberId(String type, String number);
}
