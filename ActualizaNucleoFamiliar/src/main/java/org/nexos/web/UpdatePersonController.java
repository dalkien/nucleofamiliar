package org.nexos.web;

import com.google.gson.Gson;
import jdk.nashorn.internal.runtime.arrays.ArrayIndex;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.ArrayUtils;
import org.nexos.dto.Familiar;
import org.nexos.dto.GenericResponse;
import org.nexos.dto.PersonUpdate;
import org.nexos.entity.*;
import org.nexos.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.ACCEPTED;

/**
 * Autor: Omar Madrigal
 */

@Controller
@RequestMapping("/api/familyCore")
@Slf4j
public class UpdatePersonController {

    public final AuditProcessService processService;
    public final PersonService personService;
    public final FamiliarInformationService informationService;
    public final KindsRelationshipService kindsRelationshipService;
    public final RelationshipService relationshipService;

    private Date startDate;

    @Autowired
    public UpdatePersonController(AuditProcessService processService, PersonService personService,
                                  FamiliarInformationService informationService,
                                  KindsRelationshipService kindsRelationshipService,
                                  RelationshipService relationshipService){
        this.processService = processService;
        this.personService = personService;
        this.informationService = informationService;
        this.kindsRelationshipService = kindsRelationshipService;
        this.relationshipService = relationshipService;
    }

    @PutMapping("/{type}/{number}")
    @ResponseStatus(ACCEPTED)
    public GenericResponse UpdatePerson(@PathVariable String type,
                                        @PathVariable String number,
                                        @RequestBody PersonUpdate personUpdate){
        GenericResponse response = new GenericResponse("00","OK","00");
        try {
            validateAndUpdatePerson(type, number, personUpdate);
        }catch (Exception e){
            createGenericResponse(response,"99","99", e.getLocalizedMessage());
        }
        return response;
    }


    /**
     * metodo que paso por referencia para generar salida de respuesta generica
     */
    private void createGenericResponse(GenericResponse response, String code, String error, String desc){
        response.setReturnCode(code);
        response.setErrorCode(error);
        response.setDescriptionCode(desc);
    }

    /**
     * metodo que realiza asignacion de datos de proceso para trazabilidad
     * sin importar cual sea el resultado de la ejecucion
     */
    private void createTrazability(String type, String number,
                                   Object personCreate ,
                                   GenericResponse personResponse,
                                   Exception ex){
        AuditProcess process = new AuditProcess();
        Gson st = new Gson();
        Gson rq = new Gson();
        Gson rs = new Gson();
        String status = "{\"status\":\"ok\"}";
        System.out.println(status);
        String requestA = "{ \"type\": \" "+ type+ "\" , \"number\": \""+ number + "\","+rq.toJson(personCreate)+" }";
        String request = rq.toJson(personCreate);
        System.out.println(request);
        String response = rs.toJson(personResponse);
        System.out.println(response);
        String error = ex!= null ? st.toJson(ex): st.toJson("{\"status\":\"ok\"}");
        process.setTimeAudit(new Timestamp(new Date().getTime()));
        process.setTimeStart(this.startDate);
        process.setRequestData(request);
        process.setResponseData(response);
        process.setExcepcionData(error);
        this.processService.trazability(process);
    }

    /**
     * metodo que realiza la validacion de datos con respecto a los datos de insercion
     * @param type
     * @param number
     * @return
     */
    private GenericResponse validateAndUpdatePerson(String type, String number, PersonUpdate personUpdate){
        GenericResponse response = new GenericResponse("00","OK","00");
        try {
            Optional<Person> personOptional = this.personService.getPersonalByTypeAndNumber(type, number);
            if (!personOptional.isPresent()){
                createGenericResponse(response,"10","10", "nucleo o persona no encontrado");
                return response;
            }
            personOptional.get().getRelationship().forEach(x -> {
                personUpdate.getFamiliars().forEach(y ->{
                    if (y.getNumberDocument().equals(x.getFamiliarInformation().getNumberId())){
                         updateOrCreateFamiliar("U",x.getFamiliarInformation(),y);
                    }else {
                        updateOrCreateFamiliar("I",x.getFamiliarInformation(),y);
                        Relationship relationship = new Relationship();
                        relationship.setTypeIdPrincipal(type);
                        relationship.setNumberIdPrincipal(number);
                        relationship.setTypeIdFamily(y.getTypeDocument());
                        relationship.setNumberIdFamily(y.getNumberDocument());
                        this.relationshipService.createRelationship(relationship);
                    }});
            });
            if (!response.getReturnCode().equals("00")){
                return response;
            }
            response = updatePerson(type, number, personUpdate);
        }catch (Exception e){
            createGenericResponse(response,"99","99", e.getLocalizedMessage());
        }
        return response;
    }


    private GenericResponse updatePerson(String type, String number, PersonUpdate personUpdate){
        GenericResponse response = new GenericResponse();
        try {
            Optional<Person> per = this.personService.getPersonalByTypeAndNumber(type, number);
            Person person = per.get();
            person.setCivilState(personUpdate.getStatus());
            person.setAge(Integer.parseInt(personUpdate.getAge()));
            person.setFirstName(personUpdate.getFirstName());
            person.setLastName(personUpdate.getLastName());
            person.getAdditionalInfo().setTelephone(personUpdate.getTelephone());
            person.getAdditionalInfo().setEmail(personUpdate.getEmail());
            person.getAdditionalInfo().setAddress(personUpdate.getAddress());
            this.personService.updatePerson(person);
        }catch (Exception e){
            createGenericResponse(response,"99","99", e.getLocalizedMessage());
        }
        return response;
    }
    private GenericResponse updateOrCreateFamiliar(String accion,FamiliarInformation x, Familiar y){
        GenericResponse response = new GenericResponse();
        try {
            FamiliarInformation fam  ;
            fam = x!=null ? x: new FamiliarInformation();
            fam.setLastName(y.getLastName());
            fam.setFirstName(y.getFirstName());
            fam.setAge(Integer.parseInt(y.getAge()));
            Optional<KindsRelationship> kind= this.kindsRelationshipService.consultKind(y.getRelationship());
            fam.setCodeRelationship(kind.isPresent() ? kind.get().getCodeRelationship(): "und");
            if (accion.equals("I")){
                fam.setTypeId(y.getTypeDocument());
                fam.setNumberId(y.getNumberDocument());
                this.informationService.updateInfo(fam);
            }else {
                this.informationService.createFamiliar(fam);
            }
        }catch (Exception e){
            createGenericResponse(response,"99","99", e.getLocalizedMessage());
        }
        return response;
    }

}
