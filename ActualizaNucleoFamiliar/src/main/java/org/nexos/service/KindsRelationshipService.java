package org.nexos.service;

import org.nexos.entity.KindsRelationship;
import org.nexos.repo.KindsRelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class KindsRelationshipService {
    private final KindsRelationshipRepository repository;

    @Autowired
    public KindsRelationshipService(KindsRelationshipRepository repository) {
        this.repository = repository;
    }

    public Optional<KindsRelationship> consultKind(String desc){
        return this.repository.findByDescriptionRelationship(desc);
    }
}
