package org.nexos.service;

import org.nexos.entity.FamiliarInformation;
import org.nexos.repo.FamiliarInformationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class FamiliarInformationService {
    private final FamiliarInformationRepository repository;
    @Autowired
    public FamiliarInformationService(FamiliarInformationRepository repository) {
        this.repository = repository;
    }

    public Optional<FamiliarInformation> consultFamiliar(String type, String number){
        return this.repository.findByTypeIdAndNumberId(type, number);
    }

    public void createFamiliar(FamiliarInformation familiar){
        this.repository.save(familiar);
    }

    public void updateInfo(FamiliarInformation familiar){
        this.repository.save(familiar);
    }

}
