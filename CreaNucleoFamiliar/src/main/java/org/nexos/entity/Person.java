package org.nexos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "person")
public class Person implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    //@SequenceGenerator(name = "person_id_generator", sequenceName = "person_id_seq")
    //@GeneratedValue(generator = "person_id_generator")
    @Column(name = "id_person")
    private int idPerson;

    @Column(nullable = false, name = "type_id")
    private String typeId;

    @Column(nullable = false, name = "number_id")
    private String numberId;

    @Column(nullable = false, name = "first_name")
    private String firstName;

    @Column(nullable = false, name = "last_name")
    private String lastName;

    @Column(nullable = false,name = "age")
    private int age;

    @Column(nullable = false, name ="civil_state" )
    private String civilState;


}
