package org.nexos.service;

import org.nexos.entity.AuditProcess;
import org.nexos.repo.AuditProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AuditProcessService {
    private final AuditProcessRepository auditProcessRepository;

    @Autowired
    public AuditProcessService(AuditProcessRepository auditProcessRepository){
        this.auditProcessRepository = auditProcessRepository;
    }

    public void trazability(AuditProcess auditProcess){
        this.auditProcessRepository.save(auditProcess);
    }
}
