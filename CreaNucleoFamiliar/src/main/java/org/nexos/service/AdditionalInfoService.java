package org.nexos.service;

import org.nexos.entity.AdditionalInfo;
import org.nexos.repo.AdditionalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AdditionalInfoService {
    private final AdditionalInfoRepository repository;

    @Autowired
    public AdditionalInfoService(AdditionalInfoRepository repository) {
        this.repository = repository;
    }

    public void createAdditionalInfo(AdditionalInfo additionalInfo){
        this.repository.save(additionalInfo);
    }
}
