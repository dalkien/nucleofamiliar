package org.nexos.service;

import org.nexos.entity.Person;
import org.nexos.repo.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class PersonService {
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    public Optional<Person> getPersonalByTypeAndNumber(String type, String number){
        return this.personRepository.findByTypeIdAndNumberId(type,number);
    }
    public Person createPerson(Person person){
        this.personRepository.save(person);
        return getPersonalByTypeAndNumber(person.getTypeId(), person.getNumberId()).get();
    }
}
