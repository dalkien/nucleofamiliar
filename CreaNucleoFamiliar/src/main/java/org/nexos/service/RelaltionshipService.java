package org.nexos.service;

import org.nexos.entity.Relationship;
import org.nexos.repo.RelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RelaltionshipService {
    private final RelationshipRepository repository;

    @Autowired
    public RelaltionshipService(RelationshipRepository repository) {
        this.repository = repository;
    }

    public void createRelationship(Relationship relationship){
        this.repository.save(relationship);
    }
}
