package org.nexos.repo;

import org.nexos.entity.KindsRelationship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KindsRelationshipRepository extends JpaRepository<KindsRelationship,Long> {
    Optional<KindsRelationship> findByCodeRelationship(String code);
    Optional<KindsRelationship> findByDescriptionRelationship(String desc);
}
