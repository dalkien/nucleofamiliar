package org.nexos.web.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.nexos.dto.Familiar;
import org.nexos.dto.GenericResponse;
import org.nexos.dto.PersonCreate;
import org.nexos.entity.*;
import org.nexos.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Autor: Omar Madrigal
 */
@RestController
@RequestMapping("/api/familyCore")
@Slf4j
public class CreatePersonController {

    public final PersonService personService;

    public final AuditProcessService processService;

    public final FamiliarInformationService informationService;

    public final AdditionalInfoService additionalInfoService;

    public final KindsRelationshipService kindsRelationshipService;

    public final RelaltionshipService relaltionshipService;

    private Date startDate;

    @Autowired
    public CreatePersonController (PersonService personService,AuditProcessService processService,
                                   FamiliarInformationService informationService,
                                   AdditionalInfoService additionalInfoService,
                                   KindsRelationshipService kindsRelationshipService,
                                   RelaltionshipService relaltionshipService){
        this.personService = personService;
        this.processService = processService;
        this.informationService = informationService;
        this.additionalInfoService = additionalInfoService;
        this.kindsRelationshipService = kindsRelationshipService;
        this.relaltionshipService = relaltionshipService;
    }

    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public GenericResponse createFamilyCore(@RequestBody PersonCreate personCreate){
        GenericResponse response = new GenericResponse("00","OK","00");
        this.startDate = new Date();
         try{
            response = validateInsertData(personCreate);
            if (!response.getReturnCode().equals("00")){
                createTrazability(personCreate, response,null);
                return response;
            }
            response = insertData(personCreate);
            createTrazability(personCreate, response,null);
         }catch (Exception e){
             createGenericResponse(response,"","",e.getLocalizedMessage());
             createTrazability(personCreate, response,e);
         }
        return response;
    }


    /**
     * metodo que paso por referencia para generar salida de respuesta generica
     */
    private void createGenericResponse(GenericResponse response, String code, String error, String desc){
        response.setReturnCode(code);
        response.setErrorCode(error);
        response.setDescriptionCode(desc);
    }

    /**
     * metodo que realiza asignacion de datos de proceso para trazabilidad
     * sin importar cual sea el resultado de la ejecucion
     */
    private void createTrazability(Object personCreate , GenericResponse personResponse,
                                    Exception ex){
        AuditProcess process = new AuditProcess();
        Gson st = new Gson();
        Gson rq = new Gson();
        Gson rs = new Gson();
        String status = "{\"status\":\"ok\"}";
        System.out.println(status);
        String request = rq.toJson(personCreate);
        System.out.println(request);
        String response = rs.toJson(personResponse);
        System.out.println(response);
        String error = ex!= null ? st.toJson(ex): st.toJson("{\"status\":\"ok\"}");
        process.setTimeAudit(new Timestamp(new Date().getTime()));
        process.setTimeStart(this.startDate);
        process.setRequestData(request);
        process.setResponseData(response);
        process.setExcepcionData(error);
        this.processService.trazability(process);
    }

    /**
     * metodo que valida cada uno de los elementos a ser cargados esto en caso de que genere error
     * retorna para identificar cada uno de los elementos
     */

    private GenericResponse validateInsertData(PersonCreate personCreate){
        GenericResponse response = new GenericResponse("00","OK","00");
        try {
            Optional<Person> perVal = this.personService.getPersonalByTypeAndNumber(
                    personCreate.getTypeDocument(),personCreate.getNumberDocument());
            if (perVal.isPresent()){
                createGenericResponse(response,"20","10","persona ya existe");
            }
        }catch (Exception e){
            createGenericResponse(response,"99","90",e.getLocalizedMessage());
            createTrazability(personCreate, response,e);
        }
        return response;
    }

    /**
     * metodo que realiza el alta de la informacion para cargar a la persona y su nucleo familiar
     * @param personCreate
     * @return
     */

    private GenericResponse insertData(PersonCreate personCreate){
        GenericResponse response = new GenericResponse("00", "OK", "00");
        try {
            response= createPerson(personCreate);
            if (!response.getReturnCode().equals("00")){
                return response;
            }
            response = createFamiliars(personCreate.getFamiliars());
            if (!response.getReturnCode().equals("00")){
                return response;
            }
            personCreate.getFamiliars().forEach(x->
                {
                    Relationship rela = new Relationship();
                    rela.setTypeIdPrincipal(personCreate.getTypeDocument());
                    rela.setNumberIdPrincipal(personCreate.getNumberDocument());
                    rela.setTypeIdFamily(x.getTypeDocument());
                    rela.setNumberIdFamily(x.getNumberDocument());
                    this.relaltionshipService.createRelationship(rela);
                }
            );
        }catch (Exception e){
            createGenericResponse(response,"99","90",e.getLocalizedMessage());
            createTrazability(personCreate, response,e);
        }
        return response;
    }

    /**
     * metodo que crea la persona y datos adicionales
     * @param personCreate
     * @return respuesta generica
     */
    private GenericResponse createPerson(PersonCreate personCreate ){
        GenericResponse response = new GenericResponse("00", "OK", "00");
        try {
            AdditionalInfo additionalInfo = new AdditionalInfo();
            Person person = new Person();
            person.setTypeId(personCreate.getTypeDocument());
            person.setNumberId(personCreate.getNumberDocument());
            person.setFirstName(personCreate.getFirstName());
            person.setLastName(personCreate.getLastName());
            person.setAge(Integer.parseInt(personCreate.getAge()) );
            person.setCivilState(personCreate.getStatus());
            Person insPerson = this.personService.createPerson(person);
            additionalInfo.setAddress(personCreate.getAddress());
            additionalInfo.setEmail(personCreate.getEmail());
            additionalInfo.setTelephone(personCreate.getTelephone());
            additionalInfo.setIdPerson(insPerson.getIdPerson());
            this.additionalInfoService.createAdditionalInfo(additionalInfo);
        }catch (Exception e){
            createGenericResponse(response,"99","90",e.getLocalizedMessage());
            createTrazability(personCreate, response,e);
        }
        return response;
    }

    /**
     * metodo que da de alta a todos los datos de familiares relacionados
     * @param familiars
     * @return
     */
    private GenericResponse createFamiliars(List<Familiar> familiars){
        GenericResponse response = new GenericResponse("00","OK","00");
        try {
            for (Familiar fam: familiars){
                Optional<FamiliarInformation> valFam = this.informationService
                        .consultFamiliar(fam.getTypeDocument(),fam.getNumberDocument());
                if (!valFam.isPresent()){
                    FamiliarInformation insFamiliar = new FamiliarInformation();
                    insFamiliar.setAge(Integer.parseInt(fam.getAge()));
                    Optional<KindsRelationship> kind = this.kindsRelationshipService.consultKind(fam.getRelationship());
                    String relation = kind.isPresent() ? kind.get().getCodeRelationship() : "und" ;
                    insFamiliar.setCodeRelationship(relation);
                    insFamiliar.setTypeId(fam.getTypeDocument());
                    insFamiliar.setNumberId(fam.getNumberDocument());
                    insFamiliar.setFirstName(fam.getFirstName());
                    insFamiliar.setLastName(fam.getLastName());
                    this.informationService.createFamiliar(insFamiliar);
                }
            }
        }catch (Exception e){
            createGenericResponse(response,"99","90",e.getLocalizedMessage());
            createTrazability(familiars, response,e);
        }
        return response;
    }
}
