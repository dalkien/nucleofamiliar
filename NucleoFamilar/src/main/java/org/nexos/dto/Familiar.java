package org.nexos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Familiar {
    private String typeDocument;
    private String numberDocument;
    private String fullName;
    private String relationship;
    private String age;
}
