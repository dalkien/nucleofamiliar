package org.nexos.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonResponse {

    private String typeDocument;
    private String numberDocument;
    private String fullName;
    private String age;
    private String address;
    private String telephone;
    private String status;
    private List<Familiar> familiars;
    private GenericResponse response;
}
