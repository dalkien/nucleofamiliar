package org.nexos.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "kinds_relationship")
public class kindsRelationship implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false, name = "id_relationship")
    private int idRelationship;

    @Column(nullable = false,name = "code_relationship")
    private String codeRelationship;

    @Column(nullable = false, name = "description_relationship")
    private String descriptionRelationship;

}
