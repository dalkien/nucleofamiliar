package org.nexos.web.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.nexos.dto.Familiar;
import org.nexos.dto.GenericResponse;
import org.nexos.dto.PersonResponse;
import org.nexos.entity.AuditProcess;
import org.nexos.entity.Person;
import org.nexos.service.AuditProcessService;
import org.nexos.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
/**
 * Autor: Omar Madrigal
 */
@RestController
@RequestMapping("/api/familyCore")
@Slf4j
public class PersonController {
    public final PersonService personService;

    public final AuditProcessService processService;
    @Autowired
    public PersonController (PersonService personService,
                             AuditProcessService processService){
        this.personService = personService;
        this.processService = processService;
    }

    /**
     * metodo get que recibe por parametros del path informacion para la consulta, las variables son:
     *  1) tipo -> es el tipo de documento que tiene asociado el usuario a consultar
     *  2) número -> es el numero a valor del documento
     *
     *  en el retorno es una clase response la cual retorna codigo y mensaje en caso de error
     */
    @GetMapping("/{type}/{number}")
    public PersonResponse getFamilyCore(@PathVariable String type, @PathVariable String number){
        PersonResponse personResponse = new PersonResponse();
        AuditProcess process = new AuditProcess();
        Date starDate = new Date();
        GenericResponse response = new GenericResponse("00","OK","00");
        try {
            // llamado de service para retorno de informacion en caso de ser encontrada
            Optional<Person> person = this.personService.getPersonalByTypeAndNumber(type,number);
            if (person.isPresent()){
                // si encuentra informacion se hace el mapeo de el DTO de respuesta
                createDto(person, personResponse);
                personResponse.setResponse(response);
                createTrazability(process, type, number, personResponse, starDate, null);
                this.processService.trazability(process);
            }else {
                createGenericResponse(response, "10","10","Sin datos para usuario");
                personResponse.setResponse(response);
                createTrazability(process, type, number, personResponse,starDate, null);
                this.processService.trazability(process);
            }
        }catch (Exception e){
            e.printStackTrace();
            createGenericResponse(response, "99","90",e.getLocalizedMessage());
            personResponse.setResponse(response);
            createTrazability(process, type, number, personResponse, starDate, e);
            this.processService.trazability(process);
        }
        return personResponse;
    }

    /**
     * metodo que asigna informacion, en paso por referencia de objetos
     */
    private void createDto(Optional<Person> person, PersonResponse personResponse){
        personResponse.setTypeDocument(person.get().getTypeId());
        personResponse.setNumberDocument(person.get().getNumberId());
        personResponse.setFullName(person.get().getFirstName() + person.get().getLastName());
        personResponse.setStatus(person.get().getCivilState());
        personResponse.setTelephone(person.get().getAdditionalInfo().getTelephone());
        personResponse.setAge(String.valueOf(person.get().getAge()));
        personResponse.setAddress(person.get().getAdditionalInfo().getAddress());
        List<Familiar> familiars = new ArrayList<>();
        // funcion fñecha que retorna objeto con listado de datos a almacenar en familiares asociados
        person.get().getRelationship().forEach(x -> {
            Familiar familiar = new Familiar();
            familiar.setAge(String.valueOf(x.getFamiliarInformation().getAge()));
            familiar.setFullName(x.getFamiliarInformation().getFirstName() +
                    x.getFamiliarInformation().getLastName());
            familiar.setRelationship(x.getFamiliarInformation().getKindsRelationship()
                    .getDescriptionRelationship());
            familiar.setTypeDocument(x.getFamiliarInformation().getTypeId());
            familiar.setNumberDocument(x.getFamiliarInformation().getNumberId());
            familiars.add(familiar);
        });
        personResponse.setFamiliars(familiars);
    }

    /**
    * metodo que paso por referencia para generar salida de respuesta generica
    */
    private void createGenericResponse(GenericResponse response, String code, String error, String desc){
        response.setReturnCode(code);
        response.setErrorCode(error);
        response.setDescriptionCode(desc);
    }

    /**
     * metodo que realiza asignacion de datos de proceso para trazabilidad
     * sin importar cual sea el resultado de la ejecucion
     */
    private void createTrazability(AuditProcess process, String type,
                                   String number, PersonResponse personResponse,
                                   Date starDate, Exception ex){
        Gson st = new Gson();
        Gson rq = new Gson();
        Gson rs = new Gson();
        String status =  st.toJson(personResponse.getResponse());
        System.out.println(status);
        String request = "{ \"type\": \" "+ type+ "\" , \"number\": \""+ number + "\"}";
        System.out.println(request);
        String response = rs.toJson(personResponse);
        System.out.println(response);
        String error = ex!= null ? st.toJson(ex): st.toJson(personResponse.getResponse());
        process.setTimeAudit(new Date());
        process.setTimeStart(starDate);
        process.setRequestData(request);
        process.setResponseData(response);
        process.setExcepcionData(error);

    }
}
